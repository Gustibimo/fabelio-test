## FABELIO TEST

# Requirement
- Python 3.16
- psycopg2
- gspread
- oauth2client

## Setup

- install library dependencies
```
> pip install -r requirement.txt
```

- Set environment variables

open file .env and edit with your environment:

```
DB_NAME= [database_name]
DB_HOST=localhost
DB_PORT=5432
DB_PASS=[your_database_password]
DB_USER=[your_database_username]
```

## Usage


# Task 1

Run python script to parse data and persist to postgres database:

```
>>> python task1.py
```

# Task 2

Solution for task number 2 is in file `task2.sql`

* additional assumption: We dont include the case where CS admin doesnt reply


# Task 3

For task 3 you need to get your google credentials in google API:

1. Enable google sheets API and create credentials:
`https://console.developers.google.com/apis/api/sheets.googleapis.com/credentials?project=bastion-271307`

2. Download .json file and paste file path to following code:

`credentials = ServiceAccountCredentials.from_json_keyfile_name('[your_credential_json_file_path]', scope)`

3. Run python script `task3.py`:

```
>>> python task3.py
```

4. Ouput for task 3 is like screenshot in `doc/fabelio_task3.png`


