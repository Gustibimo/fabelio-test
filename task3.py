import  gspread
from oauth2client.service_account import ServiceAccountCredentials
import psycopg2
from dotenv import load_dotenv
import os
import datetime

load_dotenv()

scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']


credentials = ServiceAccountCredentials.from_json_keyfile_name('bastion-271307-034a06b8c2dd.json', scope)

gc = gspread.authorize(credentials)
wks = gc.open("fabelio-test").sheet1

DB_NAME = os.getenv('DB_NAME')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')
DB_PASS = os.getenv('DB_PASS')
DB_USER = os.getenv('DB_USER')

con = psycopg2.connect(database=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST, port=DB_PORT)
cur = con.cursor()

for line in open("task2.sql"):
    cur.execute(line)
    con.commit()
    result = cur.fetchall()
    result = list(result)

date_values = []
avg_values = []

for row in result:
    date_values.append(row[0])


for row in result:
    avg_values.append(row[1])

date_list = wks.range('A1:A17')
avg_list = wks.range('B1:B17')

for i, val in enumerate(date_values):  
    date_list[i].value = val

for i, val in enumerate(avg_values):  
    avg_list[i].value = val

wks.update_cells(date_list)
wks.update_cells(avg_list)