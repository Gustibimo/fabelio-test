# Load up JSON Function
import json
import psycopg2
import os
from dotenv import load_dotenv

input_file = open ('./data/data.json')
json_array = json.load(input_file)

DB_NAME = os.getenv('DB_NAME')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')
DB_PASS = os.getenv('DB_PASS')
DB_USER = os.getenv('DB_USER')

con = psycopg2.connect(database=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST, port=DB_PORT)

print("Database opened successfully")

cur = con.cursor()
cur.execute("""
CREATE TABLE IF NOT EXISTS csagent (
data_loaded text,
ticket_id text,
ticket_created_at timestamp,
conversation_id text,
author_id text,
body text,
created_at timestamp,
via_channel text
)
""")

con.commit()

query = "INSERT INTO csagent (data_loaded, ticket_id, ticket_created_at,conversation_id,author_id,body,created_at,via_channel) VALUES (%s,%s,%s,%s, %s, %s, %s, %s);"

for i in range(len(json_array)):
    a = json_array[i]["conversations"]
    d = json.loads(a)
    b=(str(d["conversations"])).replace("'", '"')
    
    try:
        c = json.loads(b)
        for x in range(len(c)):
            data = (json_array[i]["data_loaded"],json_array[i]["ticket_id"],json_array[i]["ticket_created_at"], c[x]["conversation_id"], c[x]["author_id"], c[x]["body"], c[x]["created_at"], c[x]["via_channel"])
            cur.execute(query, data)
            con.commit()
    except:
        pass